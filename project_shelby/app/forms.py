from django import forms
from django.db.models import Q

from app.models import Attendee
from app.utils.api import ApiClient

api = ApiClient()

class AttendeeForm(forms.ModelForm):
    class Meta:
        model= Attendee
        fields = '__all__'

    def save(self, *args, **kwargs):
        attendee = super().save(commit=False)
        attendee.name = attendee.name.upper()
        fb_username =  attendee.fb_username
        contact_number = attendee.contact_number
        query = Attendee.objects.filter(Q(fb_username=fb_username) | Q(contact_number=contact_number))
        
        if not query:
            attendee.save()
  
        # data = {}
        # data['address'] = contact_number
        # data['name'] = attendee.name
        # resp = api.send_sms(data)
        # print('response', resp)