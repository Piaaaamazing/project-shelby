from django.contrib import admin
from app.models import *

class AttendeeAdmin(admin.ModelAdmin):
    list_display = ['name', 'fb_username', 'contact_number']

admin.site.register(Attendee, AttendeeAdmin)