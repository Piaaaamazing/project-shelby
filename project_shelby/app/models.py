from django.db import models

class Attendee(models.Model):
    fb_username = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=60)

    