from django.http import HttpResponse
from django.shortcuts import render, redirect, reverse
from django.views.generic import FormView
from django.views.generic.edit import CreateView
from django.views.generic import ListView
from django.db.models import Q
from app.models import Attendee

from app.forms import *
from app.models import *


class RSVCreateView(CreateView):
    template_name = 'index.html'
    form_class = AttendeeForm

    def get_success_url(self):
        return reverse('portal:attendees')

class RSVListView(ListView):
    template_name = 'attendees.html'

    def get_queryset(self):
        queryset = Attendee.objects.filter()
        return queryset
