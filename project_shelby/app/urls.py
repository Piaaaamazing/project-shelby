from django.conf.urls import url
from .views import *

app_name = 'portal'

urlpatterns = [
    url(r'^$', RSVCreateView.as_view(), name='dashboard'),
    url('/', RSVListView.as_view(), name='attendees'),
]
