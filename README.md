## How to run this project 

### Build
Run this command to build the docker images
```bash
$ docker-compose build 
```

### Run and stop the project
Run
```bash
$ docker-compose up -d 
```

Stop
```bash
$ docker-compose stop
```

View Logs
```bash
$ docker logs -f --tail <number_of_lines> <container_name>
```

Open the web app by using this link localhost:3200
