from urllib.parse import urljoin

import requests

app_id = 'xneqFLpBaetq5idxr7cBnLtKGnRGFKpL'
app_secret = '913f1907879acb925b7280d6347b47e206d653e94f85b27c374e856cb47c666b'
passphrase = 'dealings'
message = 'Hello %s'
base_endpoint = 'https://rest.nexmo.com/sms/json'
endpoint = '/smsmessaging/v1/outbound/21583689/requests'

class ApiClient(object):

    def _request(self, endpoint, method='get', data=None):
        method = method.lower()
        url = urljoin(base_endpoint, endpoint)
        resp = requests.post(url, params=data)
        resp = getattr(requests, method)(url, params=data)
        return resp

    def msg(self, name):
        resp = message % name
        return resp 

    def send_sms(self, data):
        content = self.msg(data['name'])
        data.pop('name', None)
        data['app_id'] = app_id
        data['app_secret'] = app_secret
        data['passphrase'] = passphrase
        data['message'] = content

        if not data['contact_numer'][:2] == '63':
            data['contact_numer'] = '63' + data['contact_numer'][1:]

        print('contact_number', data['contact_number'])
        # user = self._request(endpoint, method='post', data=data)
        # user_json = user.json()
        # user_json['status_code'] = user.status_code

        return 'user_json'

    